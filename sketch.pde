/*                                             -*- mode:C++ -*-
 *
 * Sketch description: Simple demonstration of the collector
 * 
 * Sketch author: Eric Schulte
 *
 */
#include "collector.h"          // include the collector
int cface;                      // the virtual face assigned to the
                                //   collector
int ledPin = 13;                // LED connected to digital pin 13
int wait;                       // a variable for the delay time

void setup() {
  cface = collector_init();     // initialize the collector and assign
                                //   the virtual face
}

void loop() {
  wait = random(5000);          // set wait to a random number
  delay(wait);                  // wait for the assigned time
  digitalWrite(ledPin, LOW);    // sets the LED on
  // print to the collector face and the result will be automatically
  //   routed to your laptop
  facePrintf(cface, "waited %d miliseconds\n", wait);   
  delay(500);                   // wait for the assigned time 
  digitalWrite(ledPin, HIGH);   // sets the LED off
}
