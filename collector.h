/*
  collector.h
  
  send data back to a laptop
  
*/

#ifndef COLLECTOR_H
#define COLLECTOR_H

#include "SFBTypes.h"           /* For u8 */
#include "SFBConstants.h"       /* For FACE_COUNT */
#include "SFBPrintf.h"          /* For pprintf, etc */

int collector_init();

int  collector_x();
int  collector_y();

#endif /* collector_H */
