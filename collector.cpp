#include "collector.h"
#include "SFBRandom.h"          // For random(int)
#include "SFBPacket.h"          // For packetSource
#include "SFBReactor.h"         // For Body
#include "SFBPrint.h"           // For facePrint
#include "SFBPrintf.h"          // For pprintf, etc
#include "SFBAlarm.h"           // For Alarms, etc

#define MAX_DIST 100

struct Collector {
  bool initialized;
  bool sent_prefix_p;
  int  count;                 // keep track of the last update
  u32  out_face;              // the immediate face through which to send data back
  char path[MAX_DIST];        // the path back to the central scrutinizer
  int x; int y;               // (x,y) 2D coordinates
  int ind;                    // counter used by report_prefix
  // a variety of functions for reporting back to the central scrutinizer
  void update_coords();
  void report_prefix();
};
Collector collector;

char reverseStep(char step) {
  switch(step) {
  case 'f': return 'f';
  case 'l': return 'r';
  case 'r': return 'l';
  default:  pprintf("L hork on %c\n", step); return 'z';
  }
}

void Collector::update_coords() {
  int dir = 4;
  int ind = 0;
  y = 0;
  x = 0;
  while(path[ind] != '\0') {
    // turn
    switch (path[ind]) {
    case 'f': break;
    case 'l': dir = (dir - 1) % 4; break;
    case 'r': dir = (dir + 1) % 4; break;
    default: pprintf(" L hork on '%c' path to coord\n", path[ind]); return;
    }
    // step
    switch (dir) {
    case 0: ++y; break;
    case 1: ++x; break;
    case 2: --y; break;
    case 3: --x; break;
    case 4: ++y; break;
    default: pprintf("L hork on '%d' path direction\n", dir); return;
    }
    ++ind;
  }
}

void Collector::report_prefix() {
  ind = 0;
  while(path[ind] != '\0') ++ind;       // rewind to the end of the string
  while(ind > 0) {                      // then step back to front building an R packet
    --ind; facePrintf(out_face, "R%c", reverseStep(path[ind]));
  }
  facePrintf(out_face, "(%d,%d) ", x, y);
}

void noticeCollector(u8 * packet) {
  int count;
  char dir;
  int in;
  int out;
  if (packetScanf(packet, "c%d", &count) != 2) {
    pprintf("L bad '%#p'\n",packet);
    return;
  }
  if (count > collector.count) {
    collector.initialized = true;
    collector.count = count;
    collector.out_face = packetSource(packet);
    int path_ind = 0;
    char ch;
    while(packetScanf(packet, "%c", &ch)) {       // extract the return path
      if (ch != ' ') {
        collector.path[path_ind] = ch;
        ++path_ind;
      }
    }
    collector.path[path_ind] = '\0';
    collector.update_coords();                    // update (x,y) coordinates
    for (u32 f = NORTH; f <= WEST; ++f) {         // send on to neighbors
      if (collector.out_face != f) {
        if (collector.out_face == 1)      in = 2; // swap around south and east
        else if (collector.out_face == 2) in = 1;
        else                              in = collector.out_face;
        if (f == 1)                       out = 2;
        else if (f == 2)                  out = 1;
        else                              out = f;
        switch ((4 + out - in) % 4) {              // find the dir l, r, or f
        case 1: dir = 'l'; break;
        case 2: dir = 'f'; break;
        case 3: dir = 'r'; break;
        default: pprintf(" L hork %d to %d is %d\n", in, out, ((4 + out - in) % 4)); return;
        }
        facePrintf(f, "c%d %s%c\n",
                   count, collector.path, dir);    // append dir and send onward
      }
    }
  }
}

// use collector through a virtual face printer
u8 virtual_face;
void collector_print(u8 face, u8 byte) {
  if (collector.initialized) {
    if (collector.sent_prefix_p != true) {
      collector.report_prefix();
      collector.sent_prefix_p = true;
    }
    facePrintf(collector.out_face, "%c", byte);
  }
}
void collector_println(u8 face) {
  if (collector.initialized) {
    collector.sent_prefix_p = false;
    facePrintf(collector.out_face, "\n");
  }
}
const FacePrinter collector_printer = { collector_print, collector_println, 0, 0 };

int collector_x(){ return collector.x; }
int collector_y(){ return collector.y; }

// setup the collector, and return the number of our virtual face
int collector_init() {
  Collector collector;
  collector.initialized = false;
  collector.sent_prefix_p = false;
  Body.reflex('c', noticeCollector);
  // setup virtual face printer
  bool worked = faceFindFreeFace(virtual_face);
  API_ASSERT_TRUE(worked);
  faceSetPrinter(virtual_face, &collector_printer);
  return(virtual_face);
}
